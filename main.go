package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/alecthomas/kingpin"
	"gopkg.in/yaml.v2"
)

var (
	app           = kingpin.New("valet", "Personal tools.")
	b64YamlValues = app.Command("b64-yaml-values", "Convert YAML values to base64.")
)

func main() {
	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	switch cmd {
	case b64YamlValues.FullCommand():
		fatal(b64YamlValuesCmd())
	default:
		fatal(fmt.Errorf("cmd %s unrecognized", cmd))
	}
}

func b64YamlValuesCmd() error {
	yamlDoc, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return err
	}

	// Assume a flat map, where any scalar can be unmarshalled to a string
	keyVals := map[string]string{}
	if err := yaml.Unmarshal(yamlDoc, &keyVals); err != nil {
		return err
	}
	for key, val := range keyVals {
		keyVals[key] = base64.StdEncoding.EncodeToString([]byte(val))
	}
	encodedYaml, err := yaml.Marshal(keyVals)
	if err != nil {
		return err
	}
	fmt.Println(string(encodedYaml))
	return nil
}

func fatal(err error) {
	if err != nil {
		panic(err)
	}
}
